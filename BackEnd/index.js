"use strict";

var mongoose = require("mongoose");
var app = require("./app");
var port = 3800;
const { saveAdminUser } = require("./controllers/users.controller");

mongoose
  .connect("mongodb://127.0.0.1:27017/DLDB", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Se ha establecido la conexión con la base de datos");
    app.listen(port, () => {
      console.log("El servidor de express corre en el puerto: ", port);
      saveAdminUser();
    });
  })
  .catch((err) => {
    console.log("No se ha podido establecer una conexión con la base de datos");
  });
