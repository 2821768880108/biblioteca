"use strict";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var userSchema = Schema({
  documentPersonal: String,
  name: String,
  lastname: String,
  email: String,
  role: String,
  password: String,
  administrativeRole: String,
  documents: [{ type: Schema.Types.ObjectId, ref: "document" }],
});

module.exports = mongoose.model("user", userSchema);
