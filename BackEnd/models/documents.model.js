"use strict";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var documentSchema = Schema({
  author: String,
  title: String,
  edition: String,
  description: String,
  keywords: [],
  themes: [],
  currentFrecuency: String,
  copies: Number,
  documentAvailables: Number,
  specimens: Number,
  typeDocument: String,
});

module.exports = mongoose.model("document", documentSchema);
