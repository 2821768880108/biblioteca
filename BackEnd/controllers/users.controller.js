"use strict";

var User = require("../models/users.model");
var jwt = require("../services/jwt");

function login(req, res) {
  let params = req.body;

  User.findOne(
    { email: params.email, password: params.password },
    (err, queryFound) => {
      if (err) {
        res.status(500).send({ message: "Error en el servidor" });
      } else if (queryFound) {
        res.send({ user: queryFound, token: jwt.createToken(queryFound) });
      } else {
        res.send({ message: "El correo electrónico es incorrecto" });
      }
    }
  );
}

function saveUser(req, res) {
  let params = req.body;
  let user = new User();

  if (
    params.documentPersonal &&
    params.name &&
    params.lastname &&
    params.email &&
    params.password &&
    params.role
  ) {
    User.findOne(
      {
        $or: [
          { documentPersonal: params.documentPersonal },
          { name: params.name, lastname: params.lastname },
          { email: params.email },
        ],
      },
      (err, queryFound) => {
        if (err) {
          res.status(500).send({ message: "Error en el servidor" });
        } else if (queryFound) {
          res.send({ message: "Este usuario ya existe en la base de datos" });
        } else {
          user.name = params.name;
          user.lastname = params.lastname;
          user.role = params.role;
          user.administrativeRole = "user";
          user.documentPersonal = params.documentPersonal;
          user.email = params.email;
          user.password = params.password;

          user.save((err, saved) => {
            if (err) {
              res.status(500).send({ message: "Error en el servidor" });
            } else if (saved) {
              res.status(200).send({ user: saved });
            } else {
              res.status(200).send({ message: "Error en el servidor" });
            }
          });
        }
      }
    );
  } else {
    res.send({ message: "Ingrese todos los datos que se le solicitan" });
  }
}

function editUser(req, res) {
  let idU = req.params.idU;
  let params = req.body;

  User.findByIdAndUpdate(
    idU,
    {
      name: params.name,
      lastname: params.lastname,
      email: params.email,
      role: params.role,
      documentPersonal: params.documentPersonal,
      password: params.password,
    },
    { new: true },
    (err, actualized) => {
      if (err) {
        res.status(500).send({ message: "Error en el servidor" });
      } else if (actualized) {
        res.status(200).send({ user: actualized });
      } else {
        res.status(500).send({ message: "Error en el servidor" });
      }
    }
  );
}

function deleteUser(req, res) {
  let id = req.params.id;

  User.findByIdAndDelete(id, (err, deletedUser) => {
    if (err) {
      res.status(500).send({ message: "Error en el servidor" });
    } else if (deletedUser) {
      res.status(200).send({ user: "La operación ha sido exitosa" });
    } else {
      res.send({ message: "Error en el servidor" });
    }
  });
}

function listUsers(req, res) {
  let order = parseInt(req.params.order, 10);

  User.find({ administrativeRole: "user" }, (err, usersList) => {
    if (err) {
      res
        .status(500)
        .send({ message: "Error en el servidor al listar usuarios" });
    } else if (usersList) {
      res.status(200).send({ users: usersList });
    } else {
      res.status(400).send({
        message: "No se pudo realizar la operacion, intente mas tarde",
      });
    }
  }).sort({ documentPersonal: order });
}

function saveAdminUser() {
  let user2 = new User();

  User.findOne({ administrativeRole: "admin" }, (err, findAdmin) => {
    if (err) {
      console.log("Error general al guardar usuario administrador");
    } else if (findAdmin) {
      console.log("Usuario administrador creado");
    } else {
      user2.administrativeRole = "admin";
      user2.password = "admin";
      user2.email = "admin";
      user2.save((err, userAdminSaved) => {
        if (err) {
          console.log("Error general al guardar usuario");
        } else if (userAdminSaved) {
          console.log("Usuario administrador creado");
        } else {
          console.log("No se pudo completar la operación, intente más tarde");
        }
      });
    }
  });
}

module.exports = {
  login,
  saveUser,
  editUser,
  deleteUser,
  listUsers,
  saveAdminUser,
};
