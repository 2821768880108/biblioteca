"use strict";

var Document = require("../models/documents.model");
var User = require("../models/users.model");

function listDocuments(req, res) {
  Document.find({}, (err, queryFound) => {
    if (err) {
      res.status(500).send({ message: "Error en el servidor" });
    } else if (queryFound) {
      res.status(200).send({ documents: queryFound });
    } else {
      res.status(404).send({
        message: "No se pudo realizar la operacion, intente mas tarde",
      });
    }
  });
}

function orderBy(req, res) {
  let order = parseInt(req.params.order, 10);
  let typeSearch = req.params.typeSearch;

  if (typeSearch == "copies") {
    Document.find({}, (err, documentsFound) => {
      if (err) {
        res.status(500).send({ message: "Error en el servidor" });
      } else if (documentsFound) {
        res.status(200).send({ document: documentsFound });
      } else {
        res.status(400).send({
          message: "No se pudo realizar la operacion, intente mas tarde",
        });
      }
    }).sort({ copies: order });
  } else if (typeSearch == "availabilities") {
    Document.find({}, (err, documentsFound) => {
      if (err) {
        res.status(500).send({ message: "Error en el servidor" });
      } else if (documentsFound) {
        res.status(200).send({ document: documentsFound });
      } else {
        res.status(400).send({
          message: "No se pudo realizar la operacion, intente mas tarde",
        });
      }
    }).sort({ documentAvailables: order });
  }
}

function search(req, res) {
  let typeSearch = req.params.typeSearch;
  let searchDocument = req.params.searchDocument;
  if (typeSearch == "keywords") {
    Document.find(
      { keywords: { $regex: "^" + searchDocument, $options: "i" } },
      (err, documentsFound) => {
        if (err) {
          res.status(500).send({ message: "Error en el servidor" });
        } else if (documentsFound) {
          res.status(200).send({ documents: documentsFound });
        } else {
          res.status(404).send({
            message: "No se pudo realizar la operacion, intente mas tarde",
          });
        }
      }
    );
  } else if (typeSearch == "title") {
    Document.find(
      { title: { $regex: "^" + searchDocument, $options: "i" } },
      (err, documentsFound) => {
        if (err) {
          res.status(500).send({ message: "Error en el servidor" });
        } else if (documentsFound) {
          res.status(200).send({ documents: documentsFound });
        } else {
          res.status(404).send({
            message: "No se pudo realizar la operacion, intente mas tarde",
          });
        }
      }
    );
  }
}

function saveDocument(req, res) {
  let params = req.body;
  let document = new Document();

  if (
    params.author &&
    params.title &&
    params.edition &&
    params.keywords &&
    params.themes &&
    params.copies
  ) {
    Document.findOne(
      { title: params.title, author: params.author },
      (err, documentFound) => {
        if (err) {
          res.status(500).send({ message: "Error en el servidor" });
        } else if (documentFound) {
          res.send({
            message: "Este documento ya no se pude agregar por que ya existe",
          });
        } else {
          let matriz1 = params.keywords.split(",");
          let matriz2 = params.themes.split(",");

          document.author = params.author;
          document.title = params.title;
          document.edition = params.edition;
          document.description = params.description;
          document.keywords = saveWords(matriz1);
          document.themes = saveWords(matriz2);
          document.copies = params.copies;
          document.documentAvailables = params.copies;
          document.typeDocument = params.typeDocument;

          if (params.typeDocument == "libro") {
            document.currentFrecuency = "";
            document.specimens = null;
          } else {
            document.currentFrecuency = params.currentFrecuency;
            document.specimens = params.specimens;
          }

          document.save((err, savedDocument) => {
            if (err) {
              res.status(500).send({ message: "Error en el servidor" });
            } else if (savedDocument) {
              res.status(200).send({ document: savedDocument });
            } else {
              res.send({
                message: "No se pudo realizar la operacion, intente mas tarde",
              });
            }
          });
        }
      }
    );
  } else {
    res.send({ message: "Ingrese todos los datos que se le solicitan" });
  }
}

function editDocument(req, res) {
  let idD = req.params.idD;
  let params = req.body;

  Document.findByIdAndUpdate(
    idD,
    {
      title: params.title,
      author: params.author,
      edition: params.edition,
      description: params.description,
      copies: params.copies,
      documentAvailables: params.documentAvailables,
    },
    { new: true },
    (err, queryFound) => {
      if (err) {
        res.status(500).send({ message: "Error en la base de datos" });
      } else if (queryFound) {
        res.status(200).send({ document: queryFound });
      } else {
        res.send({
          message: "No se pudo realizar la operación, intente mas tarde",
        });
      }
    }
  );
}

function deleteDocument(req, res) {
  let id = req.params.id;

  Document.findByIdAndDelete(id, (err, deleted) => {
    if (err) {
      res.status(500).send({ message: "Error en el servidor" });
    } else if (deleted) {
      res.status(200).send({ document: deleted });
    } else {
      res.send({ message: "Este documento no existe" });
    }
  });
}

function documentosDeUsuario(req, res) {
  let idU = req.params.idU;
  let order = parseInt(req.params.order, 10);
  User.findById({ _id: idU }, (err, books) => {
    if (err) {
      res
        .status(500)
        .send({ message: "Error en el servidor al buscar usuario" });
    } else if (books) {
      Document.find({ _id: books.documents }, (err, findDocuments) => {
        if (err) {
          res
            .status(500)
            .send({ message: "Error en el servidor al buscar usuario" });
        } else if (findDocuments) {
          res.status(200).send({ message: findDocuments });
        } else {
          res.status(400).send({
            message: "No se pudo realizar la operacion, intente mas tarde",
          });
        }
      }).sort({ title: order });
    } else {
      res.status(400).send({
        message: "No se pudo realizar la operacion, intente mas tarde",
      });
    }
  });
}

function addDocument(req, res) {
  let idU = req.params.idU;
  let idD = req.params.idD;

  User.findOne({ _id: idU, documents: idD }, (err, userFound) => {
    if (err) {
      res
        .status(500)
        .send({ message: "Error en el servidor al buscar usuario" });
    } else if (userFound) {
      res.send({ message: "Este libro ya pertenece a su biblioteca personal" });
    } else {
      Document.findById(idD, (err, documentFound) => {
        if (err) {
          res
            .status(500)
            .send({ message: "Error en el servidor al buscar libro" });
        } else if (documentFound) {
          if (documentFound.documentAvailables > 0) {
            User.findByIdAndUpdate(
              idU,
              { $push: { documents: idD } },
              { new: true },
              (err, actualizedUser) => {
                if (err) {
                  res
                    .status(500)
                    .send({ message: "Error en el servidor al agregar libro" });
                } else if (actualizedUser) {
                  res.status(200).send({ document: actualizedUser });
                } else {
                  res.status(400).send({
                    message:
                      "No se pudo realizar la operacion, intente mas tarde",
                  });
                }
              }
            );
          } else {
            res.send({ message: "Ya no quedan copias disponibles!!!" });
          }
        } else {
          res.status(400).send({
            message: "No se pudo realizar la operacion, intente mas tarde",
          });
        }
      });
    }
  });
}

function deleteBookorMagazine(req, res) {
  let idU = req.params.idU;
  let idD = req.params.idD;

  User.findOne({ _id: idU, documents: idD }, (err, userFound) => {
    if (err) {
      res
        .status(500)
        .send({ message: "Error en el servidor al buscar usuario" });
    } else if (userFound) {
      Document.findById(idD, (err, documentFound) => {
        if (err) {
          res
            .status(500)
            .send({ message: "Error en el servidor al buscar libro" });
        } else if (documentFound) {
          User.findByIdAndUpdate(
            idU,
            { $pull: { documents: idD } },
            { new: true },
            (err, actualizedUser) => {
              if (err) {
                res
                  .status(500)
                  .send({ message: "Error en el servidor al agregar libro" });
              } else if (actualizedUser) {
                res.status(200).send({ document: actualizedUser });
              } else {
                res.status(400).send({
                  message:
                    "No se pudo realizar la operacion, intente mas tarde",
                });
              }
            }
          );
        } else {
          res.status(400).send({
            message: "No se pudo realizar la operacion, intente mas tarde",
          });
        }
      });
    } else {
      res.send({
        message: "No puede eliminar este documento porque no existe",
      });
    }
  });
}

function saveWords(words) {
  let newKeyword = [];
  for (let i = 0; i < words.length; i++) {
    newKeyword[i] = words[i];
  }
  return newKeyword;
}

module.exports = {
  listDocuments,
  orderBy,
  search,
  saveDocument,
  editDocument,
  deleteDocument,
  documentosDeUsuario,
  addDocument,
  deleteBookorMagazine,
};
