"use strict";

var express = require("express");
var documentController = require("../controllers/documents.controller");
var mdAuth = require("../middlewares/authenticated");

var api = express.Router();

api.post(
  "/saveDocument",
  mdAuth.ensureAuthAdmin,
  documentController.saveDocument
);
api.put(
  "/editDocument/:idD",
  mdAuth.ensureAuthAdmin,
  documentController.editDocument
);
api.delete(
  "/deleteDocument/:id",
  mdAuth.ensureAuthAdmin,
  documentController.deleteDocument
);
api.get("/listDocuments", documentController.listDocuments);
api.get(
  "/searchDocument/:typeSearch/:searchDocument",
  documentController.search
);
api.get("/orderBy/:typeSearch/:order", documentController.orderBy);

api.get("/myDocuments/:idU/:order", documentController.documentosDeUsuario);

api.post(
  "/addDocument/:idU/:idD",
  mdAuth.ensureAuth,
  documentController.addDocument
);
api.post(
  "/deleteDocumentUser/:idU/:idD",
  mdAuth.ensureAuth,
  documentController.deleteBookorMagazine
);

module.exports = api;
