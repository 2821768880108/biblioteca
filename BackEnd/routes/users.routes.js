"use strict";

var express = require("express");
var userController = require("../controllers/users.controller");
var mdAuth = require("../middlewares/authenticated");

var api = express.Router();

api.post("/login", userController.login);
api.post("/saveUser", mdAuth.ensureAuthAdmin, userController.saveUser);
api.put("/editUser/:idU", mdAuth.ensureAuthAdmin, userController.editUser);
api.delete(
  "/deleteUser/:id",
  mdAuth.ensureAuthAdmin,
  userController.deleteUser
);
api.get("/listUsers/:order", userController.listUsers);

module.exports = api;
