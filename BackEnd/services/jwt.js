"use strict";

var jwt = require("jwt-simple");
var moment = require("moment");
var key = "BibliotecaDiaz";

exports.createToken = (user) => {
  var payload = {
    sub: user._id,
    documentPersonal: user.documentPersonal,
    name: user.name,
    lastname: user.lastname,
    email: user.email,
    role: user.role,
    administrativeRole: user.administrativeRole,
    iat: moment().unix(),
    exp: moment().add(2, "days").unix(),
  };
  return jwt.encode(payload, key);
};
